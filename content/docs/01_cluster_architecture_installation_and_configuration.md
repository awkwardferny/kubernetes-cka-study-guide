# Cluster Architecture, Installation & Configuration

## Manage role based access control (RBAC)

Role-based access control (RBAC) is a method of regulating access to computer or network resources based on the roles of individual users within your organization.

## Use Kubeadm to install a basic cluster

## Manage a highly-available Kubernetes cluster

## Provision underlying infrastructure to deploy a Kubernetes cluster

## Perform a version upgrade on a Kubernetes cluster using Kubeadm

## Implement etcd backup and restore
