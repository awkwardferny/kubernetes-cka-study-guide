---
title: Introduction
type: docs
---

# Kubernetes CKA Study Guide

Welcome to my Kubernetes CKA Study Guide! I have compiled some resources in order to
study for the Kubernetes CKA. These resources, have mainly come from the Kubernetes
documentation, but there are also links to other references below.

## Exam Section Percentages

The CKA exam requires a score of 66% or greater in order to pass. The sections
are broken down as follows:

| Section | Percentage |
| ------- | ---------- |
| Cluster Architecture, Installation & Configuration | 25% |
| Workloads & Scheduling | 15% |
| Services & Networking | 20% |
| Storage | 10% |
| Troubleshooting | 30% |

## Important Tips

1. Learn vim

{{< hint info >}}
**Note:** `vimtutor` can be a good resource.
{{< /hint >}}

2. Learn how to search the [Kubernetes documentation](https://kubernetes.io/docs/home/)

{{< hint info >}}
**Note:** Use `command + F` and enter `kind: Pod` or `kind: PersistentVolume` to immediately go to the example yaml.
{{< /hint >}}

3. Add some quick configuration to the exam environment, and practice it

{{< hint info >}}
**Note:** .bashrc can contain 
`alias k=kubectl`
`alias v=vim`
`export do="--dry-run=client -o yaml"`
so you can use `k run nginx $do > nginx.yaml`
 {{< /hint >}}

4. Get used to the CKA environment by using [killer.sh](https://killer.sh/)

5. Skip questions you cannot solve immediately, and just go one-by-one

6. Go fast! An hour before the exam use [killercoda](https://killercoda.com/killer-shell-cka) to get into the mood

{{< hint warning >}}
**Note:** Some of these tips came from this [reddit post](https://www.reddit.com/r/kubernetes/comments/x2f61t/passed_my_cka_today_using_the_kodekloud_course/)
{{< /hint >}}

## Links

1. https://docs.linuxfoundation.org/tc-docs/certification/tips-cka-and-ckad
2. https://docs.linuxfoundation.org/tc-docs/certification/faq-cka-ckad-cks
3. https://github.com/cncf/curriculum
4. https://killer.sh/
5. https://killercoda.com/killer-shell-cka
6. https://youtu.be/8VK9NJ3pObU
7. https://kubernetes.io/docs/reference/kubectl/cheatsheet/
8. https://github.com/WahlNetwork/certified-kubernetes-administrator-cka-exam