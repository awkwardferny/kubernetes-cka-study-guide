# Kubernetes CKA Study Guide

This guide provides sources for going over and reviewing the CKA exam. It uses Hugo and GitLab Pages to generate a static website which makes it easy to navigate.

You can access the guide by visiting [https://awkwardferny.gitlab.io/kubernetes-cka-study-guide/](https://awkwardferny.gitlab.io/kubernetes-cka-study-guide/).

## Contributing

Any contributions are welcome! Simply create an issue, merge-request, etc, and I'll
take a look! You can also find me on twitter via [@awkwardferny](https://twitter.com/awkwardferny).

## Developer

You can run this locally by cloning this repo, and then running

```
$ hugo server --minify

Start building sites … 
hugo v0.102.3+extended darwin/arm64 BuildDate=unknown

                   | EN  
-------------------+-----
  Pages            | 14  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     | 78  
  Processed images |  0  
  Aliases          |  2  
  Sitemaps         |  1  
  Cleaned          |  0  

Built in 36 ms
Watching for changes in /Users/fern/Documents/Development/Personal/kubernetes-cka-study-guide/{content,themes}
Watching for config changes in /Users/fern/Documents/Development/Personal/kubernetes-cka-study-guide/config.yaml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

You'll be able to access by visiting [http://localhost:1313/](http://localhost:1313/) and any changes
made will be rendered live.